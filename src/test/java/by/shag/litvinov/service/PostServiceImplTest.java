package by.shag.litvinov.service;

import by.shag.litvinov.api.dto.PostDto;
import by.shag.litvinov.exception.EntityRepositoryException;
import by.shag.litvinov.jpa.model.Post;
import by.shag.litvinov.jpa.repository.PostRepositoryImpl;
import by.shag.litvinov.mapping.PostDtoMapperImpl;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.WARN)
class PostServiceImplTest {

    public static final Long ID = 1L;

    @Mock
    private PostRepositoryImpl postRepository;

    @Mock
    private PostDtoMapperImpl postDtoMapper;

    @InjectMocks
    private PostServiceImpl postService;

    @AfterEach
    void tearDown() {
        verifyNoMoreInteractions(postDtoMapper, postRepository);
    }

    @Test
    void create() throws EntityRepositoryException {
        Post post = mock(Post.class);
        when(postDtoMapper.map(any(PostDto.class))).thenReturn(post);

        Post created = mock(Post.class);
        when(postRepository.create(any(Post.class))).thenReturn(created);

        PostDto dto = mock(PostDto.class);
        when(postDtoMapper.mapDto(any(Post.class))).thenReturn(dto);

        PostDto postDto = mock(PostDto.class);
        PostDto result = postService.create(postDto);
        assertEquals(dto, result);

        verify(postDtoMapper).map(postDto);
        verify(postRepository).create(post);
        verify(postDtoMapper).mapDto(created);
    }

    @Test
    void findById() throws EntityRepositoryException {
        Post post = mock(Post.class);
        Optional<Post> optionalPost = Optional.of(post);
        when(postRepository.findById(anyLong())).thenReturn(optionalPost);

        PostDto dto = mock(PostDto.class);
        when(postDtoMapper.mapDto(any(Post.class))).thenReturn(dto);

        PostDto result = postService.findById(ID);

        assertEquals(dto, result);
        verify(postRepository).findById(ID);
        verify(postDtoMapper).mapDto(post);
    }

    @Test
    void findByIdWhenNoSuchPostInDb() throws EntityRepositoryException {

        when(postRepository.findById(anyLong())).thenReturn(Optional.empty());

        RuntimeException runtimeException = assertThrows(RuntimeException.class, () -> postService.findById(ID));
        assertEquals(runtimeException.getMessage(), "!EXCEPTION NO such ID!");

        verify(postRepository).findById(ID);
    }

    @Test
    void findAll() throws EntityRepositoryException {
        Post post = mock(Post.class);
        List<Post> postList = new ArrayList<>();
        postList.add(post);
        given(postRepository.findAll()).willReturn(postList);

        List<PostDto> postDtoList = new ArrayList<>();
        List<PostDto> resultList = postService.findAll();
        assertEquals(resultList, postDtoList);

        verify(postRepository).findAll();
        verify(postDtoMapper).mapListDto(postList);
    }

    @Test
    void update() throws EntityRepositoryException {
        Post post = mock(Post.class);
        when(postDtoMapper.map(any(PostDto.class))).thenReturn(post);

        Post update = mock(Post.class);
        when(postRepository.update(any(Post.class))).thenReturn(update);

        PostDto dto = mock(PostDto.class);
        when(postDtoMapper.mapDto(any(Post.class))).thenReturn(dto);

        PostDto postDto = mock(PostDto.class);
        PostDto result = postService.update(postDto);
        assertEquals(result, dto);

        verify(postDtoMapper).map(postDto);
        verify(postRepository).update(post);
        verify(postDtoMapper).mapDto(update);
    }

    @Test
    void deleteById() throws EntityRepositoryException {
        postService.deleteById(ID);
        verify(postRepository).deleteById(ID);
    }
}