package by.shag.litvinov.mapping;

import by.shag.litvinov.api.dto.PostDto;
import by.shag.litvinov.jpa.model.Post;
import org.junit.jupiter.api.Test;

import java.time.Instant;

import static org.junit.jupiter.api.Assertions.assertEquals;

class PostDtoMapperImplTest {

    public static final Long ID = 1L;
    public static final String HEADER_NAME = "Test Name";
    public static final String CONTENT_DATA = "Test Content Data";
    public static final Instant CREATED_TO_DATE = Instant.now();
    public static final Integer NUMBER_OF_COMMENTS = 1;

    private final PostDtoMapperImpl postDtoMapperImpl = new PostDtoMapperImpl();

    @Test
    void map() {
        PostDto postDto = new PostDto();
        postDto.setId(ID);
        postDto.setHeader(HEADER_NAME);
        postDto.setContent(CONTENT_DATA);
        postDto.setCreatedAt(CREATED_TO_DATE);
        postDto.setNumberOfComments(NUMBER_OF_COMMENTS);

        Post post = postDtoMapperImpl.map(postDto);
        assertEquals(post.getId(), ID);
        assertEquals(post.getHeaderName(), HEADER_NAME);
        assertEquals(post.getContentData(), CONTENT_DATA);
        assertEquals(post.getCreatedToDate(), CREATED_TO_DATE);
        assertEquals(post.getNumberOfComments(), NUMBER_OF_COMMENTS);
    }

    @Test
    void mapDto() {
        Post post = new Post();
        post.setId(ID);
        post.setHeaderName(HEADER_NAME);
        post.setContentData(CONTENT_DATA);
        post.setCreatedToDate(CREATED_TO_DATE);
        post.setNumberOfComments(NUMBER_OF_COMMENTS);

        PostDto postDto = postDtoMapperImpl.mapDto(post);

        assertEquals(postDto.getId(), ID);
        assertEquals(postDto.getHeader(), HEADER_NAME);
        assertEquals(postDto.getContent(), CONTENT_DATA);
        assertEquals(postDto.getCreatedAt(), CREATED_TO_DATE);
        assertEquals(post.getNumberOfComments(), NUMBER_OF_COMMENTS);
    }
}