package by.shag.litvinov.service;

import by.shag.litvinov.api.dto.PostDto;
import by.shag.litvinov.exception.EntityRepositoryException;

import java.util.List;

public interface PostService {

    public PostDto create(PostDto postDto) throws EntityRepositoryException;

    public PostDto findById(Long id) throws EntityRepositoryException;

    public List<PostDto> findAll() throws EntityRepositoryException;

    public PostDto update(PostDto postDto) throws EntityRepositoryException;

    public void deleteById(Long id) throws EntityRepositoryException;
}
