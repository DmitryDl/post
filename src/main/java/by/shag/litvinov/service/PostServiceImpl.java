package by.shag.litvinov.service;

import by.shag.litvinov.api.dto.PostDto;
import by.shag.litvinov.exception.EntityRepositoryException;
import by.shag.litvinov.jpa.model.Post;
import by.shag.litvinov.jpa.repository.PostRepositoryImpl;
import by.shag.litvinov.mapping.PostDtoMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PostServiceImpl implements PostService {

    @Autowired
    private PostRepositoryImpl postRepository;
    @Autowired
    private PostDtoMapper postDtoMapper;

    @Override
    public PostDto create(PostDto postDto) throws EntityRepositoryException {
        Post post = postDtoMapper.map(postDto);
        Post created = postRepository.create(post);
        return postDtoMapper.mapDto(created);
    }

    @Override
    public PostDto findById(Long id) throws EntityRepositoryException {
        Post post = postRepository.findById(id).orElseThrow(() -> new RuntimeException("!EXCEPTION NO such ID!"));
        return postDtoMapper.mapDto(post);
    }

    @Override
    public List<PostDto> findAll() throws EntityRepositoryException {
        List<Post> postList = postRepository.findAll();
        return postDtoMapper.mapListDto(postList);
    }

    @Override
    public PostDto update(PostDto postDto) throws EntityRepositoryException {
        Post post = postDtoMapper.map(postDto);
        Post update = postRepository.update(post);
        return postDtoMapper.mapDto(update);
    }

    @Override
    public void deleteById(Long id) throws EntityRepositoryException {
        postRepository.deleteById(id);
    }
}
