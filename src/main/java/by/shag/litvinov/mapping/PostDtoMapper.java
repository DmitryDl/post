package by.shag.litvinov.mapping;

import by.shag.litvinov.api.dto.PostDto;
import by.shag.litvinov.jpa.model.Post;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.ReportingPolicy;

import java.util.List;

@Mapper(unmappedTargetPolicy = ReportingPolicy.ERROR)
public interface PostDtoMapper {

    @Mapping(target = "headerName", source = "header")
    @Mapping(target = "contentData", source = "content")
    @Mapping(target = "createdToDate", source = "createdAt")
    Post map(PostDto dto);

    @InheritInverseConfiguration
    PostDto mapDto(Post post);

    List<PostDto> mapListDto(List<Post> postList);
}
