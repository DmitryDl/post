package by.shag.litvinov.api.dto;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import java.time.Instant;

@Getter
@Setter
@EqualsAndHashCode
public class PostDto {

    private Long id;
    private String header;
    private String content;
    private Instant createdAt;
    private Integer numberOfComments;
}
