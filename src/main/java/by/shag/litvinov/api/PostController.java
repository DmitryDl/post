package by.shag.litvinov.api;

import by.shag.litvinov.api.dto.PostDto;
import by.shag.litvinov.exception.EntityRepositoryException;
import by.shag.litvinov.service.PostServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@Controller
public class PostController {

    @Autowired
    private PostServiceImpl postService;

    @GetMapping("/post")
    public String findAll(Model model) throws EntityRepositoryException {
        List<PostDto> postDtoList = postService.findAll();
        model.addAttribute("posts", postDtoList);
        return "posts";
    }

    @GetMapping("post/find-by-id")
    public String getFindById(Model model, String postId) {
        model.addAttribute("postId", postId);
        return "find-by-id";
    }

    @GetMapping(value = "post/id")
    public String findById(@RequestParam(name = "postId", required = false) long postId,
                           Model model) throws EntityRepositoryException {
        Optional<PostDto> postDtoOptional = Optional.ofNullable(postService.findById(postId));
        PostDto postDto = postDtoOptional.get();
        model.addAttribute("posts", postDto);
        return "posts";
    }

    @GetMapping(value = "/post/new-post")
    public String getCreateForm(Model model) {
        model.addAttribute("post", new PostDto());
        return "post-create";
    }

    @PostMapping(value = "post")
    public String createPost(@ModelAttribute("post") PostDto postDto,
                             Model model) throws EntityRepositoryException {
        postService.create(postDto);
        return findAll(model);
    }

    @GetMapping(value = "/post/update-button")
    public String getUpdateButtonForm(Model model, PostDto postDto) {
        model.addAttribute("post", postDto);
        return "post-update-button";
    }

    @PostMapping(value = "post/update")
    public String updatePost(@ModelAttribute("post") PostDto postDto, Model model)
            throws EntityRepositoryException {
        postService.update(postDto);
        return findAll(model);
    }

    @DeleteMapping("post/delete")
    public String deleteById(@RequestParam(name = "postId", required = false) long postId,
                             Model model) throws EntityRepositoryException {
        postService.deleteById(postId);
        return findAll(model);
    }
}
