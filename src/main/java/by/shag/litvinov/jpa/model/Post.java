package by.shag.litvinov.jpa.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.time.Instant;

@Getter
@Setter
@Entity
@Table(name = "post")
public class Post {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "header", nullable = false)
    private String headerName;

    @Column(name = "content", nullable = false)
    private String contentData;

    @Column(name = "created_at", nullable = false)
    private Instant createdToDate;

    @Column(name = "number_of_comments")
    private Integer numberOfComments;
}
