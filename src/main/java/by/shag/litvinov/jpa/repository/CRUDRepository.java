package by.shag.litvinov.jpa.repository;

import by.shag.litvinov.exception.EntityRepositoryException;

import java.util.List;
import java.util.Optional;

public interface CRUDRepository<E, ID> {

    E create(E entity) throws EntityRepositoryException;

    Optional<E> findById(ID id) throws EntityRepositoryException;

    List<E> findAll() throws EntityRepositoryException;

    E update(E entity) throws EntityRepositoryException;

    void deleteById(ID id) throws EntityRepositoryException;
}
