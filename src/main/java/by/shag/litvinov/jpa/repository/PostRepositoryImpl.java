package by.shag.litvinov.jpa.repository;

import by.shag.litvinov.exception.EntityRepositoryException;
import by.shag.litvinov.jpa.model.Post;
import by.shag.litvinov.jpa.util.EntityManagerUtil;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.util.List;
import java.util.Optional;

@Repository
public class PostRepositoryImpl implements CRUDRepository<Post, Long> {

    private static final String FIND_ALL_QUERY = "SELECT n FROM Post n";

    @Override
    public Post create(Post post) throws EntityRepositoryException {
        post.setCreatedToDate(Instant.now());
        EntityManager em = EntityManagerUtil.getEntityManager();
        try {
            em.getTransaction().begin();
            em.persist(post);
            em.getTransaction().commit();
        } catch (Exception e) {
            em.getTransaction().rollback();
            throw new EntityRepositoryException("Exception in create method");
        } finally {
            em.close();
        }
        return post;
    }

    @Override
    public Optional<Post> findById(Long id) {
        EntityManager em = EntityManagerUtil.getEntityManager();
        Post post = em.find(Post.class, id);
        em.close();
        return Optional.ofNullable(post);
    }

    @Override
    public List<Post> findAll() {
        EntityManager em = EntityManagerUtil.getEntityManager();
        List<Post> resultList = em.createQuery(FIND_ALL_QUERY, Post.class).getResultList();
        em.close();
        return resultList;
    }

    @Override
    public Post update(Post post) throws EntityRepositoryException {
        EntityManager em = EntityManagerUtil.getEntityManager();
        Post persistedPost = em.find(Post.class, post.getId());
        try {
            em.getTransaction().begin();
            persistedPost.setHeaderName(post.getHeaderName());
            persistedPost.setContentData(post.getContentData());
            persistedPost.setNumberOfComments(post.getNumberOfComments());
            em.merge(persistedPost);
            em.getTransaction().commit();
        } catch (Exception e) {
            em.getTransaction().rollback();
            throw new EntityRepositoryException("Exception in update method");
        } finally {
            em.close();
        }
        return post;
    }

    @Override
    public void deleteById(Long id) throws EntityRepositoryException {
        EntityManager em = EntityManagerUtil.getEntityManager();
        Post post = em.find(Post.class, id);
        try {
            em.getTransaction().begin();
            em.remove(post);
            em.getTransaction().commit();
        } catch (Exception e) {
            em.getTransaction().rollback();
            throw new EntityRepositoryException("Exception in deleteById method");
        } finally {
            em.close();
        }
    }
}
