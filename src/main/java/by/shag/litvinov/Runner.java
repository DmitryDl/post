package by.shag.litvinov;

import by.shag.litvinov.exception.EntityRepositoryException;
import org.springframework.beans.BeansException;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

@SpringBootApplication
public class Runner implements ApplicationContextAware {

    private static ApplicationContext context;

    public static void main(String[] args) throws EntityRepositoryException {
        SpringApplication.run(Runner.class);
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        context = applicationContext;
    }
}
